// ignore_for_file: non_constant_identifier_names

import 'dart:developer';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'contact_list.dart';
// ignore: camel_case_types
class userScreen extends StatefulWidget {
  const userScreen({ Key? key, required this.title }) : super(key: key);
  
   final String title;
  @override
  State<userScreen> createState() => _userScreenState();
}

// ignore: camel_case_types
class _userScreenState extends State<userScreen> {
  @override
  Widget build(BuildContext context) {

     TextEditingController namesController = TextEditingController();
     TextEditingController phonenumberController = TextEditingController();
     TextEditingController emailaddressController = TextEditingController();
     TextEditingController addressController = TextEditingController();
       
    Future _addContacts(){
      final names = namesController.text;
      final phoneNumber = phonenumberController.text;
      final emailaddress = emailaddressController.text;
      final address = addressController.text;

       final ref = FirebaseFirestore.instance.collection("userContact").doc();
       
       return ref.set({"names":names,"phoneNumber":phoneNumber,"emailaddress":emailaddress,"address":address,"doc_Id":ref.id})
       .then((value) => {
        namesController.text = "",
        phonenumberController.text = "",
        emailaddressController.text = "",
        addressController.text = "",
       }
       )
      
       .catchError((onError) => log(onError));
       
    }
    return MaterialApp(
       // ignore: unnecessary_new
       theme: new ThemeData(
         primaryColor: Colors.green,
        
       ),
    
    home: Scaffold(
       appBar: AppBar(
       title: const Text("Contact Form") 
     ),
      body: Center(
          child: ListView(
            children: [
              Column(
                children:<Widget> [
                   const SizedBox(height: 15.0,),
                  Padding(
                    padding:  const EdgeInsets.only(bottom:8.0),
                    child: TextFormField(
                      controller: namesController,
                      decoration: const InputDecoration(
                        labelText: "Your full names",
                        fillColor: Colors.white,
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.blue,width: 2.0)
                        )
                      ),
                    ),
                  ),
                   Padding(
                    padding:  const EdgeInsets.only(bottom:8.0),
                    child: TextFormField(
                       controller: phonenumberController,
                      decoration: const InputDecoration(
                        labelText: "Enter Phone Number",
                        fillColor: Colors.white,
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.blue,width: 2.0)
                        )
                      ),
                     
                    ),
                  ),
                  Padding(
                    padding:  const EdgeInsets.only(bottom:8.0),
                    child: TextFormField(
                       controller: addressController,
                      decoration: const InputDecoration(
                        labelText: "Enter Address",
                        fillColor: Colors.white,
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.blue,width: 2.0)
                        )
                      ),
                     
                    ),
                  ),
                  Padding(
                    padding:  const EdgeInsets.only(bottom:8.0),
                    child: TextFormField(
                       controller: emailaddressController,
                      decoration: const InputDecoration(
                        labelText: "Enter Email",
                        fillColor: Colors.white,
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.blue,width: 2.0)
                        )
                      ),
                   
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
        
                    ElevatedButton(                
                  onPressed: (){
                    _addContacts();             
                  },
                  child: const Text("Submit Contact"),
                     ),
                    ],
                  )
                ],
              ),
             const SizedBox(height: 10.0),
             Contactlist(),
            
            ],
            
          ),
        
      ),
    ),
    );
  }
}
