

import 'package:flutter/material.dart';
import 'package:flutter_module_q3/screens/edit_your_profile.dart';
import 'package:flutter_module_q3/screens/feature1.dart';
import 'package:flutter_module_q3/screens/main_login.dart';
import 'package:flutter_module_q3/screens/register.dart';
import 'package:flutter_module_q3/screens/feature2.dart';
import 'user_form_database.dart';

// ignore: camel_case_types
class secondPage extends StatefulWidget {
  const secondPage({ Key? key }) : super(key: key);
  @override
  State<secondPage> createState() => _secondPageState();
}
// ignore: camel_case_types
class _secondPageState extends State<secondPage> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
       // ignore: unnecessary_new
       theme: ThemeData(
         brightness: Brightness.dark,
        
       ),
        home: Scaffold(
     appBar: AppBar(
       title: const Text("Dashboard") 
     ),
     floatingActionButton: FloatingActionButton.extended(
       label: const Text("Register"),
      onPressed: () {
       Navigator.push(context,MaterialPageRoute(builder: (context) =>const thirdPage(), ));
      },
      backgroundColor: Colors.green
     ),
     body: Center(
       child: Column(
         children: [
             const SizedBox(height: 15),
           ElevatedButton(
             child: const Text("Login"),
            onPressed: () {
             Navigator.push(context,MaterialPageRoute(builder: (context) => const mainPage(), ));
            }
            ),
            const SizedBox(height: 15),
             ElevatedButton(
             child: const Text("Register here"),
            onPressed: () {
             Navigator.push(context,MaterialPageRoute(builder: (context) => const thirdPage(), ));
            }
            ),
             const SizedBox(height: 15),
            ElevatedButton(
             child: const Text("Features"),
            onPressed: () {
               Navigator.push(context,MaterialPageRoute(builder: (context) => const forthPage(), ));
            }
            ),
               const SizedBox(height: 15),
            ElevatedButton(
             child: const Text("Updated Features"),
            onPressed: () {
                Navigator.push(context,MaterialPageRoute(builder: (context) =>  const fifthPage(), ));
            }
            ),
             const SizedBox(height: 15),
            ElevatedButton(
             child: const Text("Edit your profile"),
            onPressed: () {
                Navigator.push(context,MaterialPageRoute(builder: (context) => const sixthPage(), ));
            }
            ),  
            const SizedBox(height: 15),
            ElevatedButton(
             child: const Text("Edit your profile"),
            onPressed: () {
                Navigator.push(context,MaterialPageRoute(builder: (context) => const sixthPage(), ));
            }
            ), 
            const SizedBox(height: 15),
            ElevatedButton(
             child: const Text("User Form"),
            onPressed: () {
                Navigator.push(context,MaterialPageRoute(builder: (context) => const userScreen(title: '',), ));
            }
            ), 
         ],
       ),
     )
        ),
    );
    
  } 
}