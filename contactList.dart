
import 'dart:core';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
class Contactlist extends StatefulWidget {
  const Contactlist({ Key? key, }) : super(key: key);

  @override
  State<Contactlist> createState() => _ContactlistState(); 
}

class _ContactlistState extends State<Contactlist> {
  
  final Stream<QuerySnapshot> _myUserContacts = 
  FirebaseFirestore.instance.collection('userContact').snapshots();

  

  @override
  Widget build(BuildContext context) {
  
TextEditingController _nameFieldcntroler = TextEditingController();
    TextEditingController _phoneNumFieldcntroler = TextEditingController();
    TextEditingController _EmailFieldcntroler = TextEditingController();
    TextEditingController _AddressFieldcntroler = TextEditingController();

    void _delete(docId){
    FirebaseFirestore.instance
    .collection("userContact")
    .doc(docId)
    .delete()
    .then((value) => print("deleted"));
    }

    void _update(data){   

      var collection = FirebaseFirestore.instance.collection("userContact");
      _nameFieldcntroler.text = data["names"];
      _phoneNumFieldcntroler.text = data["phoneNumber"];
      _EmailFieldcntroler.text = data["emailaddress"];
      _AddressFieldcntroler.text = data["address"];


    showDialog(context: context, 
    builder: (_) => AlertDialog(
      title: Text("Update"),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          TextField(
        controller:_nameFieldcntroler,
        ),
        TextField(
        controller:_phoneNumFieldcntroler,
        ),
        TextField(
        controller:_EmailFieldcntroler,
        ),
        TextField(
        controller:_AddressFieldcntroler,
        ),
        TextButton(
          onPressed: (){
           collection.doc(data["doc_Id"])
           .update({
            "names": _nameFieldcntroler.text,
            "phoneNumber": _phoneNumFieldcntroler.text,
            "emailaddress": _EmailFieldcntroler.text,
            "address": _AddressFieldcntroler.text,
            

           });
           Navigator.pop(context);
          },
         child: Text("Update")),
      ]),
    )
    );
    }

    return StreamBuilder(
    
      stream: _myUserContacts,
      builder: (BuildContext context,
       AsyncSnapshot<QuerySnapshot<Object?>> snapshot){
        if (snapshot.hasError){
          return const Text("Something went wrong");
        }
          if(snapshot.connectionState == ConnectionState.waiting){
          return   const Center (child: CircularProgressIndicator());
        }
       if(snapshot.hasData){
        return Row(
          children: [
           Expanded(
            child: SizedBox(
            height: (MediaQuery.of(context).size.height),
            width: (MediaQuery.of(context).size.width),
            child: ListView(
              children: snapshot.data!.docs
              .map((DocumentSnapshot documentSnapshot)  {
                  Map<String, dynamic> data = documentSnapshot.data()! as Map<
                  String, dynamic>;
                  return Column(
                    children: [
                      Card(
                        child: Column(
                          children:[
                      ListTile(
                        title: Text(data['names']),
                         subtitle:Text(data['phoneNumber']), 
                         leading:Text(data['emailaddress']),
                         trailing:Text(data['address']),
                        
                      ),
                      ButtonTheme(
                        child: ButtonBar(
                        children:[
                          OutlineButton.icon(
                            onPressed:(){                             
                              _update(data);
                            },
                            icon: Icon(Icons.edit),
                            label: Text("Edit"),
                            ),
                             OutlineButton.icon(
                            onPressed:(){
                             _delete(data["doc_Id"]);
                            },
                            icon: Icon(Icons.remove),
                            label: Text("Delete"),
                            )
                        ],
                        ),
                      ),
                          ],
                        )
                      )
                    ],
                  );
                })
                .toList()
                   
            ),
            )
            )
          ],
        );

       }else{
        return(Text("No data"));
       }
       },
      
    );
  }
}
