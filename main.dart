
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_module_q3/screens/dashboard.dart';
import 'package:flutter_module_q3/screens/edit_your_profile.dart';
import 'package:flutter_module_q3/screens/feature1.dart';
import 'package:flutter_module_q3/screens/feature2.dart';
import 'package:flutter_module_q3/screens/main_login.dart';
import 'package:flutter_module_q3/screens/register.dart';
import 'screens/splash.dart';
import 'screens/user_form_database.dart';

Future main() async{
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: FirebaseOptions(
       apiKey: "AIzaSyC9v9WXJvNJDobRDiY0Ya-qkPO5IZDepZw",
       authDomain: "me-alone-aed58.firebaseapp.com",
       projectId: "me-alone-aed58",
       storageBucket: "me-alone-aed58.appspot.com",
       messagingSenderId: "529497628683",
       appId: "1:529497628683:web:5abe294cfd7acd1b7b670e",
       measurementId: "G-M1PPL1LKKR"
      )

    
  );
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  
  
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return  MaterialApp(
      debugShowCheckedModeBanner: false,
    theme: ThemeData(
    //primaryColor: Colors.purple,
    //hintColor: Colors.deepOrange,

    ),
    initialRoute: "/splash",
    routes: {
      "/":(context)  => const mainPage(),
      "/dashbord":(context) => const secondPage(),
      "/register":(context) => const thirdPage(),
      "/feature":(context) => const forthPage(),
      "/feature2":(context) => const fifthPage(),
      "/edit":(context) => const sixthPage(),
      "/splash":(context) => const splashScreen(),
       "/user_form":(context) => const userScreen(title: '',),
    },
    );
  }
}

